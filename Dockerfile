FROM node:14.10.0-stretch-slim
RUN env http_proxy=http://10.50.32.30:8080/ env https_proxy=http://10.50.32.30:8080/ env TERM=xterm apt-get update && env http_proxy=http://10.50.32.30:8080/ env https_proxy=http://10.50.32.30:8080/ env TERM=xterm apt-get --no-install-recommends -y install apt-utils bc procps curl whiptail libaio1 build-essential python3 && apt-get clean && rm -rf /var/lib/apt/lists/*
RUN cp /usr/share/zoneinfo/Europe/Rome /etc/localtime
RUN echo "Europe/Rome" > /etc/timezone
RUN curl --silent --output mqm_8007.tar.gz http://10.50.200.14/repository/mqclient/mqm_8007.tar.gz
RUN curl --silent --output var_mqm.tar.gz http://10.50.200.14/repository/mqclient/var_mqm.tar.gz
RUN mkdir -m 0755 -p /etc/opt/mqm
RUN curl --silent --output /etc/opt/mqm/mqinst.ini http://10.50.200.14/repository/mqclient/mqinst.ini
RUN chmod 0644 /etc/opt/mqm/mqinst.ini
RUN env TERM=xterm apt-get autoremove -y curl
RUN mkdir -m 0755 -p /opt
RUN tar -C /opt/ -xvf mqm_8007.tar.gz && rm -f /mqm_8007.tar.gz
RUN ln -s mqm_8007 /opt/mqm
RUN groupadd -g 1111 prog && useradd -r -u 1111 -g prog -d /home/arkie -m arkie
RUN groupadd -g 5203 mqm && useradd -r -u 997 -g mqm -d /var/mqm -m mqm
RUN rm -rf /var/mqm
RUN tar -C /var/ -xvf var_mqm.tar.gz && rm -f /var_mqm.tar.gz
RUN chown -R mqm:mqm /opt/mqm_8007
RUN chown -R mqm:mqm /var/mqm
RUN ln -s lib /usr/lib64
RUN env LD_LIBRARY_PATH=/lib64:/usr/lib:/opt/mqm/gskit8/lib64:/opt/mqm/lib64 env PATH=/bin:/usr/bin:/usr/local/bin:/opt/mqm/bin /opt/mqm_8007/bin/setmqinst -i -p /opt/mqm_8007
WORKDIR /home/arkie
USER arkie

COPY --chown=arkie:prog package*.json ./

RUN env http_proxy=http://10.50.32.30:8080/ env https_proxy=http://10.50.32.30:8080/ env MQIJS_NOREDIST=true npm install

COPY --chown=arkie:prog . .

RUN /home/arkie/node_modules/@nestjs/cli/bin/nest.js build

USER root
RUN env TERM=xterm apt-get autoremove -y apt-utils whiptail build-essential python3

USER arkie
ENV LD_LIBRARY_PATH /lib64:/usr/lib:/opt/mqm/gskit8/lib64:/opt/mqm/lib64
ENV PATH /bin:/usr/bin:/usr/local/bin:/opt/mqm/bin
ENV LANG en_US.UTF-8
ENV TZ Europe/Rome

EXPOSE 8080
ENTRYPOINT ["/usr/local/bin/node", "dist/main.js"]
