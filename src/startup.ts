import {QueueService} from "./queue/queue.service";

export const startup = async () => {

  await require('dotenv').config();
  const queueService:QueueService = new QueueService();
  try {
    await queueService.start();

  } catch (e) {
    console.log('Eccezione ', e);
  }
}