export const environment = {
    ADDRESS: '10.2.252.1',
    PORT: '1415',
    CHANNEL: 'MFES_TELEMATICA2',
    QUEUE_MANAGER: 'MFES',
    QUEUE: 'K8S.NJS_RIPUB_DIV.TOPIC',
    USER: 'arkie',

    SERVER_PATH: 'printer/print',
    SERVER_ADDRESS: 'http://localhost:4000',
}
