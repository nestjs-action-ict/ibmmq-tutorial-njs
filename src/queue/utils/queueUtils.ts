import { environment } from "../../conf/config";

const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');

let ok = true;
let exitCode = 0;

const mq = require('ibmmq');
const MQC = mq.MQC; // Want to refer to this export directly for simplicity
const cno = new mq.MQCNO();
const csp = new mq.MQCSP();
const cd = new mq.MQCD();

export const formatErr = (err) => {
    return  "MQ call failed in " + err.message;
}

export const configure = () => {
    cno.SecurityParms = csp;
    cno.Options |= MQC.MQCNO_CLIENT_BINDING;
    cd.ConnectionName = `${process.env.ADDRESS || environment.ADDRESS}(${process.env.PORT || environment.PORT})`;
    cd.ChannelName = `${process.env.CHANNEL || environment.CHANNEL}`;
    cno.ClientConn = cd;
}

export const connect = async ( connected ) => {
    const qMgr = `${process.env.QUEUE_MANAGER || environment.QUEUE_MANAGER}`;
    await mq.Connx(qMgr, cno, connected);
}

export const disconnect = (conn) => {
    mq.Disc(conn, function(err) {
        if (err) {
            console.log(formatErr(err));
        } else {
            console.log("MQDISC successful");
        }
    });
}

export const receiveMessages = (connection, queueName, onReceiveMessage) => {

    receiveMessage = onReceiveMessage;

    // Define what we want to open, and how we want to open it.
    var od = new mq.MQOD();
    od.ObjectName = queueName;
    od.ObjectType = MQC.MQOT_Q;

    od.AlternateUserId = `${process.env.USER || environment.USER}`;
    var openOptions = MQC.MQOO_INPUT_AS_Q_DEF
      | MQC.MQOO_ALTERNATE_USER_AUTHORITY;
    mq.Open(connection,od,openOptions,function(err,hObj) {
        if (err) {
            console.log(formatErr(err));
        } else {
            console.log("MQOPEN of %s successful",queueName);
            // And loop getting messages until done.
            getMessages(hObj);
        }
        cleanup(connection,hObj);
    });
}


// When we're done, close queues and connections
function cleanup(hConn,hObj) {
    mq.Close(hObj, 0, function(err) {
        if (err) {
            console.log(formatErr(err));
        } else {
            console.log("MQCLOSE successful");
        }
        mq.Disc(hConn, function(err) {
            if (err) {
                console.log(formatErr(err));
            } else {
                console.log("MQDISC successful");
            }
        });
    });
}


function getMessages(hObj) {
    while (ok) {
        getMessage(hObj);
    }
}


function getMessage(hObj) {

    var buf = Buffer.alloc(1024);

    var mqmd = new mq.MQMD();
    var gmo = new mq.MQGMO();

    gmo.WaitInterval = 3 * 10000; // 30 seconds
    gmo.MatchOptions = MQC.MQMO_NONE;

    gmo.Options = MQC.MQGMO_NO_SYNCPOINT |
      MQC.MQGMO_WAIT;

    mq.setTuningParameters({getLoopPollTimeMs: 500});

    mq.GetSync(hObj,mqmd,gmo,buf,function(err,len) {
        console.log('LEN: ', len);
        getCB(err, hObj, gmo, mqmd, buf, hObj);
    });
}

let receiveMessage = function(message) {
    console.log(`Message: ${message}`)
}

function getCB(err, hObj, gmo,md,buf, hConn ) {
    // If there is an error, prepare to exit by setting the ok flag to false.
    if (err) {
        if (err.mqrc == MQC.MQRC_NO_MSG_AVAILABLE) {
            console.log("No more messages available.");
        } else {
            console.log(formatErr(err));
            exitCode = 1;
        }
        ok = false;
        // We don't need any more messages delivered, so cause the
        // callback to be deleted after this one has completed.
        mq.GetDone(hObj);
    } else {
        if (md.Format=="MQSTR") {
            console.log("message <%s>", decoder.write(buf));
            receiveMessage(decoder.write(buf))
        } else {
            console.log("binary message: " + buf);
            receiveMessage(buf)
        }
    }
}
