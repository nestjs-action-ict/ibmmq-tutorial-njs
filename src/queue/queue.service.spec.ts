import { Test, TestingModule } from '@nestjs/testing';
import { QueueService } from './queue.service';
import {configure, connect, disconnect, receiveMessages} from "./utils/queueUtils";
import {environment} from "../conf/config";

jest.mock('./utils/queueUtils')
jest.mock('axios')

describe('QueueService', () => {
  let service: QueueService;


  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [QueueService],
    }).compile();

    service = module.get<QueueService>(QueueService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    service.start();
  });

  it('should configure and call the connect method when start is called', () => {
    service.start();
    expect(configure).toBeCalled();
    expect(connect).toBeCalled();
  })

  it('should receive messages', ()=> {
    service.receive();
    expect(receiveMessages).toBeCalled();
    expect(receiveMessages).toBeCalledWith(undefined, environment.QUEUE, service.onReceiveMessage)
  })

  it('should call disconnect when stop', ()=> {
    service.stop();
    expect(disconnect).toBeCalled();
  })

});
