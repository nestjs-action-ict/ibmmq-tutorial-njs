import { configure, connect, formatErr, disconnect, receiveMessages } from './utils/queueUtils';
import {environment} from "../conf/config";

import axios from "axios";

export class QueueService {
    connection: any;

    async start() {
        console.log('configuring')
        configure();
        console.log('configured')
        const self = this;
        await connect( (err, conn) => {
            if(err) {
                console.log('Error connecting...')
                console.log( formatErr(err));
            } else {
                console.log('setting local connection');
                self.connection = conn;
                console.log('Connection successful');
                this.receive();
            }
        })
    }

    async stop() {
        console.log('Disconnecting');
        disconnect(this.connection);
    }

    async receive() {
         receiveMessages(this.connection, process.env.QUEUE || environment.QUEUE, this.onReceiveMessage)
    };

    onReceiveMessage(message) {
        // TODO: Modificare il codice con l'effettiva implementazione della funzionalità
        console.log(`Message in service: ${message}`);
        axios.post(`${process.env.SERVER_ADDRESS || environment.SERVER_ADDRESS}/${process.env.SERVER_PATH || environment.SERVER_PATH}`, {
            message: message,
            from: 'IbmMQ'
        }).then(function (result){
            console.log('Message delivered')
        }, function (err){
            console.log(err)
        });
    }
}
