import { Module } from '@nestjs/common';
import { QueueModule } from './queue/queue.module';
import { NestFactory } from '@nestjs/core';

@Module({
  imports: [QueueModule],
})
export class AppModule {}

async function bootstrap() {
  const app = await NestFactory.createMicroservice(AppModule);
  app.listen(() => console.log('Microservice is listening'));
}

bootstrap();
